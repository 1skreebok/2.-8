# Задача №2, урок 8
# создаем переменную для ввода списка чисел строкой
num = [int(i) for i in input().split()]
# создаем 2 переменную - список для сбора результатов
num2 = []
# создаем условие: если длина списка больше 2
if len(num) > 2:
# создаем цикл проверки iго эл-та в диапазоне длины списка
    for i in range(len(num)):
# условие добавления суммы соседних элементов во второй список для первого элемента введенного списка
        if i == 0:
            num2.append(num[-1] + num[i + 1])
# условие добавления суммы соседних элементов во второй список для НЕпервого и не последнего элемента введенного списка
        elif i != 0 and i != len(num) - 1:
            num2.append(num[i - 1] + num[i+1])
        elif i == len(num) - 1:
            num2.append(num[i - 1] + num[0])
    print(*num2)
# создаем условие: если длина списка 2 числа
elif len(num) == 2:
    num2.append(num[1] + num[-1])
    num2.append(num[0] + num[0])
    print(*num2)
# создаем условие: если длина списка 1 число
else:
    print(*num)
